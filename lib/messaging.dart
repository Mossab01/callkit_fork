import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:untitled/call_kit.dart';
import 'package:untitled/notification_model.dart';

import 'fcm.dart';
import 'keys.dart';
import 'local_notification.dart';
import 'notification_middleware.dart';

class Messaging {
  static String? token;

  static deleteToken() {
    Messaging.token = null;
    FCM.deleteRefreshToken();
  }

  @pragma('vm:entry-point')
  static Future<void> onNotificationReceived(RemoteMessage message) async {
    await Firebase.initializeApp();
    debugPrint(message.data.toString());

    // await LocalNotification.initializeLocalNotification(
    //   icon: 'ic_notification',
    // );
    // LocalNotification.showNotification(
    //   payload: message.data,
    //   notification: RemoteNotification(
    //     title: message.data['title'],
    //     body: message.data['body'],
    //   ),
    // );
    Callkit.showCall(
        FCMNotificationModel(callerName: message.notification?.body));
  }

  @pragma('vm:entry-point')
  static initFCM() async {
    print('Habibi');
    try {
      if (await Permission.notification.status !=
          PermissionStatus.permanentlyDenied) {
        await Permission.notification.request();
      }
      await FCM.initializeFCM(
        withLocalNotification: false,
        navigatorKey: Keys.navigatorKey,
        onNotificationReceived: onNotificationReceived,
        onNotificationPressed: LocalNotification.onNotificationPressed,
        onTokenChanged: (String? token) {
          if (token != null) {
            Messaging.token = token;

            if (kDebugMode) {
              print('FCM token  $token');
            }
          }
        },
        // add this icon to android/app/src/main/res/drawable/ic_notification.png
        icon: 'ic_notification',
      );
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
  }
}
