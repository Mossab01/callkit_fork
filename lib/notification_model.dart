import 'notification_type.dart';

class FCMNotificationModel {
  String? callerPhotoPath;
  int? meetingId;
  String? serverLink;
  String? time;
  NotificationType? type;
  String? room;
  String? callerName;
  bool? isFree;
  int? mediaId;
  String? userType;
  String? callType;
  int? mediaReplayId;

  FCMNotificationModel(
      {this.callerPhotoPath,
      this.meetingId,
      this.serverLink,
      this.time,
      this.type,
      this.room,
      this.isFree,
      this.mediaId,
      this.callerName});

  FCMNotificationModel.fromJson(Map<String, dynamic> json) {
    meetingId =
        json['meetingId'] == null ? null : int.tryParse(json['meetingId']);
    serverLink = json['serverLink'];
    time = json['time'];
    type = NotificationType.values[int.parse(json['type'] ?? '0')];
    room = json['room'];
    callerName = json['callerName'] ?? '';
    userType = json['userType'] ?? '';
    callType = json['callType'] ?? '';
    callerPhotoPath = json['callerPhotoPath'] ?? '';
    mediaId = json['mediaId'] == null ? null : int.parse(json['mediaId']);
    mediaReplayId =
        json['mediaReplayId'] == null ? null : int.parse(json['mediaReplayId']);

    //  isFree =MyConverter.toBoolean( json['isFree']?? 'false') ;
    if (callerName!.trim().isEmpty) callerName = null;
    if (callerPhotoPath!.trim().isEmpty) callerPhotoPath = null;
  }

  FCMNotificationModel.fromSignalR(Map<String, dynamic> json) {
    meetingId = json['notification']['data']['properties']['MeetingId'];
    serverLink = json['notification']['data']['properties']['ServerLink'];
    room = json['notification']['data']['properties']['Room'];
    mediaId = json['mediaId'] == null ? null : int.parse(json['mediaId']);
    mediaReplayId =
        json['mediaReplayId'] == null ? null : int.parse(json['mediaReplayId']);
    time = json['time'];
    type = NotificationType.values.firstWhere((element) => element
        .toString()
        .toLowerCase()
        .contains(
            json['notification']['notificationName'].toString().toLowerCase()));

    callerName = json['notification']['data']['properties']['CallerName'] ?? '';
    callerPhotoPath =
        json['notification']['data']['properties']['CallerPhotoPath'] ?? '';
    userType = json['notification']['data']['properties']['UserType'] ?? '';
    callType = json['notification']['data']['properties']['CallType'] ?? '';
    //  isFree =MyConverter.toBoolean( json['isFree']?? 'false') ;
    if (callerName!.trim().isEmpty) callerName = null;
    if (callerPhotoPath!.trim().isEmpty) callerPhotoPath = null;
  }
}
