import 'dart:async';

import 'package:flutter_callkit_incoming/entities/android_params.dart';
import 'package:flutter_callkit_incoming/entities/call_event.dart';
import 'package:flutter_callkit_incoming/entities/call_kit_params.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:is_lock_screen2/is_lock_screen2.dart';

import 'package:uuid/uuid.dart';

import 'keys.dart';
import 'notification_model.dart';

class Callkit {
  static FCMNotificationModel fcm = FCMNotificationModel();

  @pragma('vm:entry-point')
  static showCall(FCMNotificationModel fcm) async {
    try {

      FlutterCallkitIncoming.onEvent.listen((CallEvent? event) async {
        if (Keys.navigatorKey.currentContext == null) {
          Timer.periodic(
            const Duration(milliseconds: 5000),
            (timer) {
              if (Keys.navigatorKey.currentState != null) timer.cancel();
            },
          );
        }

        switch (event!.event) {
          case Event.actionCallAccept:
            bool reply = false;
            int count = 30;
            Timer.periodic(const Duration(seconds: 1), (timer) async {
              reply = !(await isLockScreen())!;
              count--;
              if (count == 0) timer.cancel();
              if (reply) {
                await FlutterCallkitIncoming.endAllCalls();
                timer.cancel();
              }
            });

            break;
          case Event.actionDidUpdateDevicePushTokenVoip:
          // TODO: Handle this case.
          case Event.actionCallIncoming:
          // TODO: Handle this case.
          case Event.actionCallStart:
          // TODO: Handle this case.
          case Event.actionCallDecline:
          // TODO: Handle this case.
          case Event.actionCallEnded:
          // TODO: Handle this case.
          case Event.actionCallTimeout:
          // TODO: Handle this case.
          case Event.actionCallCallback:
          // TODO: Handle this case.
          case Event.actionCallToggleHold:
          // TODO: Handle this case.
          case Event.actionCallToggleMute:
          // TODO: Handle this case.
          case Event.actionCallToggleDmtf:
          // TODO: Handle this case.
          case Event.actionCallToggleGroup:
          // TODO: Handle this case.
          case Event.actionCallToggleAudioSession:
          // TODO: Handle this case.
          case Event.actionCallCustom:
          // TODO: Handle this case.
        }
      });
    } catch (e) {
      print(e);
    }

    String id = const Uuid().v4();

    var params = CallKitParams(

      id: id,
      nameCaller: fcm.callerName,
      appName: 'BMS',
      handle: '',
      type: 1,
      duration: 3600000,
      avatar: 'gg',
      textAccept: 'OK',
      textDecline: 'OK',
      // textMissedCall: 'Missed call',
      // textCallback: '',
      extra: <String, dynamic>{'userId': '1a2b3c4d'},
      headers: <String, dynamic>{'apiKey': 'Abc@123!', 'platform': 'flutter'},
      android: const AndroidParams(
        isCustomNotification: false,
        isShowLogo: true,
        // isShowCallback: false,
        // isShowMissedCallNotification: false,
        ringtonePath: 'run',
        backgroundColor: '#0088cc',
      ),
    );

    // var t = <String, dynamic>{
    //   'appName': 'Firework',
    //   'id': fcm.room ?? id,
    //   'nameCaller': fcm.callerName ?? 'Caller name',
    //   'handle': '${fcm.userType!} ( ${fcm.callType!} )',
    //   'avatar': fcm.callerPhotoPath,
    //   'textCallback': '',
    //   'textMissedCall': 'Missed call',
    //   'type': 2,
    //   'duration': 31000,
    //   'extra': <String, dynamic>{'userId': fcm.callerName ?? 'Caller name'},
    //   'ios': <String, dynamic>{'handleType': 'number'},
    //   'android': <String, dynamic>{
    //     'isShowMissedCallNotification': false,
    //     'isShowCallback': false,
    //     'isCustomNotification': false,
    //     'isShowLogo': true,
    //     'ringtonePath': 'ringtone_default',
    //     'backgroundColor': '#0088cc',
    //     // 'background': 'https://i.pravatar.cc/500',
    //     // 'actionColor': '#4CAF50'
    //   },
    // }; //

    await FlutterCallkitIncoming.showCallkitIncoming(params);
  }

  @pragma('vm:entry-point')
  static endCall(FCMNotificationModel fcm) {
    FlutterCallkitIncoming.endCall(fcm.room!);
  }
}
