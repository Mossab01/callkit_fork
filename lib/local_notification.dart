import 'dart:async';
import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:untitled/notification_model.dart';

import 'notification_middleware.dart';

class LocalNotification {
  /// Create a [AndroidNotificationChannel] for heads up notifications
  static AndroidNotificationChannel channel = const AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    // 'This channel is used for important notifications.', // description
    importance: Importance.high,
  );

  /// Initialize the [FlutterLocalNotificationsPlugin] package.
  static final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static initializeLocalNotification({required String icon}) async {
    // Create an Android notification Channel.
    ///
    /// We use this channel in the `AndroidManifest.xml` file to override the
    /// default FCM channel to enable heads up notifications.
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings(icon);
    DarwinInitializationSettings initializationSettingsIOS =
        const DarwinInitializationSettings(
            onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);

    final NotificationAppLaunchDetails? details =
        await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
    if (details?.notificationResponse != null &&
        (details?.didNotificationLaunchApp ?? false)) {
      Timer.periodic(
        const Duration(milliseconds: 500),
        (timer) {
          if (!initialized) return;
          backgroundHandler(details!.notificationResponse!);
          timer.cancel();
        },
      );
    }
    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveBackgroundNotificationResponse: backgroundHandler,
      onDidReceiveNotificationResponse: (notificationResponse) {
        LocalNotification.onDidReceiveNotificationResponse(
            notificationResponse: notificationResponse,
            onData: onNotificationPressed);
      },
    );
  }

  static bool initialized = false;

  static backgroundHandler(notificationResponse) {
    onDidReceiveNotificationResponse(
      notificationResponse: notificationResponse,
      onData: onNotificationPressed,
    );
  }

  static void onDidReceiveNotificationResponse(
      {required NotificationResponse notificationResponse, onData}) async {
    final String? payload = notificationResponse.payload;
    if (notificationResponse.payload != null) {
      debugPrint('notification payload: $payload');
      var jsonData = jsonDecode(payload!);
      onData(jsonData);
    }
  }

  static onNotificationPressed(Map<String, dynamic> data) {
    NotificationMiddleware.forward(FCMNotificationModel.fromJson(data));
  }

  static Future onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    debugPrint(title);
  }

  static showNotification(
      {required RemoteNotification notification,
      Map<String, dynamic>? payload,
      String? icon}) {
    flutterLocalNotificationsPlugin.show(
      notification.hashCode,
      notification.title,
      notification.body,
      NotificationDetails(
        android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          channelDescription: channel.description,
          icon: icon,
        ),
      ),
      payload: jsonEncode(payload),
    );
  }
}
