import 'dart:async';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_core/firebase_core.dart';
import 'local_notification.dart';

class FCM {
  FCM._();

  static late ValueChanged<String?> _onTokenChanged;

  static initializeFCM(
      {required void Function(String? token) onTokenChanged,
      void Function(Map<String, dynamic> data)? onNotificationPressed,
      required BackgroundMessageHandler onNotificationReceived,
      GlobalKey<NavigatorState>? navigatorKey,
      required String icon,
      bool withLocalNotification = true}) async {
    _onTokenChanged = onTokenChanged;
    await LocalNotification.initializeLocalNotification(icon: icon);
    await Firebase.initializeApp(
        options: const FirebaseOptions(
      apiKey: 'AIzaSyC5cQApXb5KzgWhmwoEk03lbaBPBKgQFVs',
      appId: '1:500538527277:android:96ce681c0fa6e84dad72a4',
      messagingSenderId: '500538527277',
      projectId: 'bms-notifier',
      storageBucket: 'bms-notifier.appspot.com',
    ));
    FirebaseMessaging.instance.getToken().then(onTokenChanged);
    Stream<String> tokenStream = FirebaseMessaging.instance.onTokenRefresh;
    tokenStream.listen(onTokenChanged);

// Set the background messaging handler early on, as a named top-level function
    FirebaseMessaging.onBackgroundMessage(onNotificationReceived);

    /// Update the iOS foreground notification presentation options to allow
    /// heads up notifications.
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    final message = await FirebaseMessaging.instance.getInitialMessage();

    if (message != null) {
      if (navigatorKey != null) {
        Timer.periodic(
          const Duration(milliseconds: 500),
          (timer) {
            if (navigatorKey.currentState == null) return;
            onNotificationPressed!(message.data);
            timer.cancel();
          },
        );
      }
    }

    FirebaseMessaging.onMessage.listen(
      (RemoteMessage message) {
        debugPrint('A new onMessage event was published!');

        onNotificationReceived(message);
        RemoteNotification? notification = message.notification;
        AndroidNotification? android = message.notification?.android;

        if (notification != null && android != null && withLocalNotification) {
          LocalNotification.showNotification(
              notification: notification, payload: message.data, icon: icon);
        }
      },
    );

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      debugPrint('A new onMessageOpenedApp event was published!');
      onNotificationPressed!(message.data);
    });

    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) async {
      debugPrint('A new onBackgroundMessage event was published!');
      onNotificationPressed!(message.data);
      onNotificationReceived(message);
    });
  }

//static Future<void> _firebaseMessagingBackgroundHandler
  static deleteRefreshToken() {
    FirebaseMessaging.instance.deleteToken();
    FirebaseMessaging.instance.getToken().then(_onTokenChanged);
  }
}
