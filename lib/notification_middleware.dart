import 'call_kit.dart';
import 'notification_model.dart';

class NotificationMiddleware {
  static forward(FCMNotificationModel notification) async {}

  static onReceived(FCMNotificationModel notification) async {
    Callkit.showCall(notification);
  }
}
