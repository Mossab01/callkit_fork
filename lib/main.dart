import 'dart:io';

import 'package:flutter/material.dart';
import 'package:untitled/home_app.dart';

import 'keys.dart';
import 'messaging.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (Platform.isAndroid || Platform.isIOS) await Messaging.initFCM();

  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: Keys.navigatorKey,
      title: 'Fire Alert',
      home: const HomeApp(),
    );
  }
}
